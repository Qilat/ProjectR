package fr.projectr.core.client.model.chat;

import fr.projectr.api.utils.IModule;
import fr.projectr.core.client.model.command.CommandManager;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.view.ui.ChatUI;
import lombok.Getter;

import java.util.LinkedList;

/**
 * Created by Qilat on 13/09/2017.
 */
public class ChatManager implements IModule {

    private static final int CHAT_SIZE = 50;
    @Getter
    private static LinkedList<Message> msgList = new LinkedList<>();

    private static ChatManager manager;

    public static ChatManager get() {
        return manager;
    }

    @Override
    public boolean init() {
        manager = this;
        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    public static void analyzeMsgAndAct(Message msg) {
        if (msg.isCommand()) {
            CommandManager.get().runCommand(msg.getSender(), msg.getCommandInput());
        } else {
            ChatManager.addMessage(msg);
            ChatUI.updateChat();
        }
    }


    public static void addMessage(Message msg) {
        if (msgList.size() >= CHAT_SIZE)
            msgList.removeFirst();
        msgList.addLast(msg);
    }

    public static String getRenderString() {
        String renderStr = "";
        for (Message msg : msgList) {
            switch (msg.getType()) {
                case INFO:
                    renderStr += (msg.toString() + "\n");
                    break;
                case DEFAULT:
                    renderStr += ("[" + msg.getSender().getName() + "] " + msg.toString() + "\n");
                    break;
            }

        }
        return renderStr;
    }
}
