package fr.projectr.core.client.model.command;

import fr.projectr.core.client.model.entity.CommandSender;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 14/08/2017.
 */
public abstract class Command {

    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String name;

    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String[] alias;

    public Command(String name, String... alias) {
        this.setName(name);
        this.setAlias(alias);
    }


    /**
     * Method which is called when someone use the attached command
     *
     * @param sender Entity who send command
     * @param cmd    Command called
     * @param input  Array of string inputed : first slot in array occupied by used alias
     * @return true if correct command - false if not
     */
    public abstract boolean execute(CommandSender sender, Command cmd, String[] input);


}
