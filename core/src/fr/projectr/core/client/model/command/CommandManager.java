package fr.projectr.core.client.model.command;

import fr.projectr.api.utils.IModule;
import fr.projectr.core.client.model.entity.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Theophile on 11/11/2018 for ProjectRClient.
 */
public class CommandManager implements IModule {

    private static CommandManager manager;

    private ArrayList<Command> commands = new ArrayList<>();

    @Override
    public boolean init() {
        manager = this;
        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    public static CommandManager get() {
        return manager;
    }


    public void registerCommand(Command command) {
        if (command != null) {
            commands.add(command);
        }
    }

    public boolean runCommand(CommandSender sender, String input) {
        String[] inputArray = input.split(" ");
        for (Command cmd : commands) {
            if (inputArray[0].equals(cmd.getName()) || Arrays.asList(cmd.getAlias()).contains(inputArray[0])) {
                if (cmd.execute(sender, cmd, inputArray))
                    return true;
            }
        }
        return false;
    }

    public ArrayList<String> getHelp() {
        ArrayList<String> help = new ArrayList<>();

        for (Command cmd : commands)
            help.add(cmd.getName());

        return help;
    }

}
