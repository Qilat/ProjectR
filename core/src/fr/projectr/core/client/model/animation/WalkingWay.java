package fr.projectr.core.client.model.animation;


/**
 * Created by Qilat on 02/09/2017.
 */
public enum WalkingWay {
    BACK(0),
    LEFT(1),
    FRONT(2),
    RIGHT(3);

    private int id;

    WalkingWay(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}
