package fr.projectr.core.client.model.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Qilat on 19/07/2017.
 */
public abstract class Animation {

    com.badlogic.gdx.graphics.g2d.Animation<TextureRegion> animation;
    Texture sheet;
    float stateTime;
    private int FRAME_COLS = 0,
            FRAME_ROWS = 0;

    public Animation(String path, int cols, int rows) {
        this.FRAME_COLS = cols;
        this.FRAME_ROWS = rows;

        sheet = new Texture(Gdx.files.internal(path));

        TextureRegion[][] tmp = TextureRegion.split(sheet, sheet.getWidth() / FRAME_COLS, sheet.getHeight() / FRAME_ROWS);

        TextureRegion[] walkFrames = new TextureRegion[FRAME_ROWS * FRAME_COLS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                walkFrames[index++] = tmp[i][j];
            }
        }

        animation = new com.badlogic.gdx.graphics.g2d.Animation<TextureRegion>(0.25f, walkFrames);

        stateTime = 0f;

    }

    public void render(SpriteBatch batch, int x, int y) {
        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion currentFrame = animation.getKeyFrame(stateTime, true);
        batch.draw(currentFrame, x, y);
    }

    public int getFRAME_ROWS() {
        return FRAME_ROWS;
    }

    public int getFRAME_COLS() {
        return FRAME_COLS;
    }

    public Texture getSheet() {
        return sheet;
    }

    public com.badlogic.gdx.graphics.g2d.Animation<TextureRegion> getAnimation() {
        return animation;
    }

}
