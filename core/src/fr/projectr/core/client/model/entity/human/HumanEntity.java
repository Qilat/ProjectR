package fr.projectr.core.client.model.entity.human;

import com.badlogic.gdx.physics.box2d.*;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.model.entity.Entity;
import fr.projectr.core.client.view.game.actor.HumanEntityActor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Theophile on 18/11/2018 for ProjectR.
 */
public abstract class HumanEntity extends Entity {

    @Getter
    @Setter
    protected HumanEntityActor actor;

    @Setter
    @Getter
    private Body body;

    public HumanEntity(UUID uuid, String name) {
        super(uuid, name);
    }

    public HumanEntity(UUID uuid) {
        this(uuid, null);
    }

    @Override
    public void sendMessage(Message msg) {
    }

    @Override
    public HumanEntityActor getActor() {
        return this.actor;
    }

    public void setPosition(float x, float y) {
        if (this.getBody() != null)
            this.setupBody(this.getBody().getWorld(), x, y);
    }

    public void setupBody(World world, float x, float y) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.fixedRotation = true;
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.45f, 0.5f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 1f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;
        body.createFixture(fixtureDef);

        this.setBody(body);

        polygonShape.dispose();
    }

}
