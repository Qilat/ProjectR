package fr.projectr.core.client.model.entity.human;

import com.badlogic.gdx.physics.box2d.*;
import fr.projectr.core.client.model.entity.NPCDesc;
import fr.projectr.core.client.view.game.actor.HumanEntityActor;
import lombok.Getter;

import java.util.UUID;

/**
 * Created by Theophile on 18/11/2018 for ProjectR.
 */
public class NPCHumanEntity extends HumanEntity {

    @Getter
    private NPCDesc desc;

    public NPCHumanEntity(UUID uuid, NPCDesc desc) {
        super(uuid, desc.getName());
        this.setActor(new HumanEntityActor(this, desc.getSkin(), desc.getColumns()));
        this.desc = desc;
    }

    public void updateNPC() {
        this.name = desc.getName();

        this.getActor().setColums(desc.getColumns());
        this.getActor().setTexture(desc.getSkin());
        this.getActor().reloadTexture();

        this.setPosition(desc.getPosition().x, desc.getPosition().y);
    }

    @Override
    public void setupBody(World world, float x, float y) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x, y);
        bodyDef.fixedRotation = true;
        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(0.45f, 0.5f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = polygonShape;
        fixtureDef.density = 1f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;
        body.createFixture(fixtureDef);

        this.setBody(body);

        polygonShape.dispose();
    }
}
