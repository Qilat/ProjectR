package fr.projectr.core.client.model.entity.virus;

/**
 * Created by Qilat on 18/09/2017 for ProjectRClient.
 */
public enum ProgramLang {
    C,
    CPP,
    JAVA,
    PYTHON,
    BASH;
}
