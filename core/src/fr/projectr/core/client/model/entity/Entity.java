package fr.projectr.core.client.model.entity;

import com.badlogic.gdx.scenes.scene2d.Actor;
import fr.projectr.core.client.model.entity.property.EntityExtendedProperty;
import fr.projectr.core.client.utils.datainfo.Info;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Qilat on 15/08/2017.
 */
public abstract class Entity extends Info<EntityExtendedProperty> implements CommandSender {

    @Setter
    @Getter
    public String name = "Entity";

    /**
     * Constructor of Entity.
     */
    public Entity(UUID uuid) {
        super(uuid);
    }

    /**
     * Constructor of Entity.
     *
     * @param name Name of the entity
     */
    public Entity(UUID uuid, String name) {
        super(uuid);
        this.name = name;
    }

    @Override
    public void addProperties(Collection<String> propertiesTag) {

    }

    @Override
    protected void saveOneProperty(EntityExtendedProperty property) {

    }

    @Override
    protected void loadOneProperty(EntityExtendedProperty property, String propertyData) {

    }

    public abstract Actor getActor();
}
