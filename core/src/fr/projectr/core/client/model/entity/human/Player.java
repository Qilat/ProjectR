package fr.projectr.core.client.model.entity.human;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import fr.projectr.core.client.model.animation.WalkingWay;
import fr.projectr.core.client.model.chat.ChatManager;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.model.entity.property.EntityExtendedProperty;
import fr.projectr.core.client.view.game.actor.PlayerActor;
import fr.projectr.core.client.view.ui.ChatUI;
import lombok.Getter;

import java.util.Collection;
import java.util.UUID;

import static fr.projectr.core.client.Constants.PLAYER_WALK_SPEED;

/**
 * Created by Qilat on 15/08/2017.
 */
public class Player extends HumanEntity {

    @Getter
    private PlayerActor playerActor;

    public Player(UUID uuid, String name) {
        super(uuid, name);
        this.playerActor = new PlayerActor(this);
        this.setActor(this.playerActor);
    }

    public Player(UUID uuid) {
        this(uuid, "PlayerActor");
    }

    @Override
    public void addProperties(Collection<String> propertiesTag) {

    }

    @Override
    protected void saveOneProperty(EntityExtendedProperty property) {

    }

    @Override
    protected void loadOneProperty(EntityExtendedProperty property, String propertyData) {

    }

    @Override
    public void sendMessage(Message msg) {
        ChatManager.addMessage(msg);
        ChatUI.updateChat();
    }

    public void handleInputMovement() {
        this.getBody().setLinearVelocity(0, 0);

        boolean left = Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A),
                right = Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D),
                down = Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S),
                up = Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W);

        if (left || right || down || up)
            this.getPlayerActor().setMoving(true);

        if (up && !down & !left & !right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.BACK);
        } else if (!up && down && !left && !right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.FRONT);
        } else if (!up && !down && left && !right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.LEFT);
        } else if (!up && !down && !left && right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.RIGHT);
        } else if (up && left && !down && !right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.BACK);
        } else if (up && right && !down && !left) {
            this.getPlayerActor().setCurrentWay(WalkingWay.BACK);
        } else if (down && left && !up && !right) {
            this.getPlayerActor().setCurrentWay(WalkingWay.FRONT);
        } else if (down && right && !up && !left) {
            this.getPlayerActor().setCurrentWay(WalkingWay.FRONT);
        } else if (up && down && !left && !right) {
            this.getPlayerActor().setMoving(false);
            up = false;
            down = false;
        } else if (!up && !down && left && right) {
            this.getPlayerActor().setMoving(false);
            left = false;
            right = false;
        } else if (!up && !down & !left & !right) {
            this.getPlayerActor().setMoving(false);
        }

        Vector2 pos = this.getBody().getPosition();

        if (left) {
            this.getBody().applyLinearImpulse(-PLAYER_WALK_SPEED, 0, pos.x, pos.y, true);
        }
        if (right) {
            this.getBody().applyLinearImpulse(PLAYER_WALK_SPEED, 0, pos.x, pos.y, true);
        }
        if (down) {
            this.getBody().applyLinearImpulse(0, -PLAYER_WALK_SPEED, pos.x, pos.y, true);
        }
        if (up) {
            this.getBody().applyLinearImpulse(0, PLAYER_WALK_SPEED, pos.x, pos.y, true);
        }

    }

}
