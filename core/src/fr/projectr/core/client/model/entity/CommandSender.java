package fr.projectr.core.client.model.entity;

import fr.projectr.core.client.model.console.message.Message;

/**
 * Created by Qilat on 14/08/2017.
 */
public interface CommandSender {

    void sendMessage(Message msg);

    String getName();
}
