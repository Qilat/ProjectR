package fr.projectr.core.client.model.entity.virus;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.model.entity.Entity;
import lombok.Getter;

import java.util.UUID;

/**
 * Created by Qilat on 17/09/2017 for ProjectRClient.
 */
public class Virus extends Entity {

    @Getter
    private ProgramLang programLang;
    @Getter
    private VirusType type;
    @Getter
    private Texture smallWormTexture = new Texture("texture/ui/worm.png");
    @Getter
    private Image smallImage;
    @Getter
    private Texture profilWormTexture = new Texture("texture/ui/wormphotoprofil.png");
    @Getter
    private Image profilImage;


    public Virus(UUID uuid, String name, ProgramLang programLang, VirusType type) {
        super(uuid, name);
        this.programLang = programLang;
        this.type = type;
        this.smallImage = new Image(smallWormTexture);
        this.profilImage = new Image(profilWormTexture);
    }

    @Override
    public void sendMessage(Message msg) {
    }


    @Override
    public Actor getActor() {
        return null;
    }
}
