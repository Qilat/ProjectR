package fr.projectr.core.client.model.entity.property;

import fr.projectr.core.client.model.entity.Entity;
import fr.projectr.core.client.utils.datainfo.property.ExtendedProperty;

/**
 * Created by Theophile on 14/11/2018 for ProjectRClient.
 */
public class EntityExtendedProperty extends ExtendedProperty<Entity> {

    @Override
    public void preLoad() {

    }

    @Override
    public void postLoad() {

    }

    @Override
    public void preSave() {

    }

    @Override
    public void postSave() {

    }

    @Override
    public String getPropertyTag() {
        return null;
    }
}
