package fr.projectr.core.client.model.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import fr.projectr.core.client.model.animation.WalkingWay;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Theophile on 18/11/2018 for ProjectR.
 */
public class NPCDesc {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Texture skin;
    @Getter
    @Setter
    private int columns;
    @Getter
    @Setter
    private Vector2 position;
    @Getter
    @Setter
    private WalkingWay orientation;

    public NPCDesc(String name, Texture skin, int columns, Vector2 position, WalkingWay orientation) {
        this.name = name;
        this.skin = skin;
        this.columns = columns;
        this.position = position;
        this.orientation = orientation;
    }
}
