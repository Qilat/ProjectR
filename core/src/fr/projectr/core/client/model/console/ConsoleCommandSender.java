package fr.projectr.core.client.model.console;

import fr.projectr.api.API;
import fr.projectr.core.client.model.console.message.Message;
import fr.projectr.core.client.model.entity.CommandSender;
import lombok.Getter;

/**
 * Created by Qilat on 14/08/2017.
 */
public class ConsoleCommandSender implements CommandSender {

    @Getter
    private String name;

    public ConsoleCommandSender(String name) {
        super();
        this.name = name;
    }

    @Override
    public void sendMessage(Message msg) {
        API.getLogger().info(msg);
    }
}
