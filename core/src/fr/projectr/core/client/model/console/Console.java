package fr.projectr.core.client.model.console;

import fr.projectr.api.API;
import fr.projectr.api.utils.lang.Lang;
import fr.projectr.core.client.model.command.CommandManager;
import org.quartz.*;

import java.util.HashMap;
import java.util.Scanner;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

/**
 * Created by Qilat on 17/08/2017.
 */
public class Console extends ConsoleCommandSender implements Job {
    private static Scanner scanner = new Scanner(System.in);

    public Console() {
        super("Console");
    }

    static JobDetail getJob() {
        return newJob(Console.class)
                .withIdentity("consoleJob", "group1")
                .build();
    }

    static Trigger getTrigger() {
        return TriggerBuilder.newTrigger()
                .withIdentity("consoleTrig", "group1")
                .startNow()
                .withSchedule(simpleSchedule())
                .build();
    }

    @Override
    public void execute(JobExecutionContext context) {
        while (Console.scanner.hasNextLine()) {
            String input = Console.scanner.nextLine();

            if (!CommandManager.get().runCommand(this, input)) {
                HashMap<String, String> metadata = new HashMap<>();
                metadata.put("input", input);
                API.getLogger().info(Lang.get("tryied_cmd_does_nothing", metadata));
            }
        }
    }
}
