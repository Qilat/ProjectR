package fr.projectr.core.client.model.console;

import fr.projectr.api.utils.IModule;
import fr.projectr.core.client.controller.console.EchoCommand;
import fr.projectr.core.client.controller.console.LanguageCommand;
import fr.projectr.core.client.controller.console.StopCommand;
import fr.projectr.core.client.controller.console.VisuCommand;
import fr.projectr.core.client.model.command.CommandManager;
import lombok.Getter;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

/**
 * Created by Theophile on 09/11/2018 for ProjectRClient.
 */
public class ConsoleManager implements IModule {

    @Getter
    private static ConsoleManager instance;
    @Getter
    private Scheduler scheduler;

    public ConsoleManager(Scheduler scheduler) {
        ConsoleManager.instance = this;
        this.scheduler = scheduler;
    }

    @Override
    public boolean init() {
        try {
            scheduler.scheduleJob(Console.getJob(), Console.getTrigger());
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void registerCommands() {
        CommandManager.get().registerCommand(new StopCommand());
        CommandManager.get().registerCommand(new EchoCommand());
        CommandManager.get().registerCommand(new LanguageCommand());
        CommandManager.get().registerCommand(new VisuCommand());
    }

    @Override
    public void stop() {

    }
}
