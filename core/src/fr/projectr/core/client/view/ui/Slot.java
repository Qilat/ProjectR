package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import fr.projectr.core.client.model.entity.virus.Virus;
import lombok.Getter;

import java.util.ArrayList;

/**
 * Created by Qilat on 18/09/2017 for ProjectRClient.
 */
public class Slot extends WidgetGroup {
    public static Slot S_0, S_1, S_2, S_3, S_4, S_5;
    private static ArrayList<Slot> slotList = new ArrayList<>();

    static {
        S_0 = new Slot(0, 200 / 5, 400 - 120 * 1);
        S_2 = new Slot(2, 200 / 5, 400 - 120 * 2);
        S_4 = new Slot(4, 200 / 5, 400 - 120 * 3);

        S_1 = new Slot(1, (200 / 5) * 3, 400 - 120 * 1);
        S_3 = new Slot(3, (200 / 5) * 3, 400 - 120 * 2);
        S_5 = new Slot(5, (200 / 5) * 3, 400 - 120 * 3);
    }

    @Getter
    private Texture slotTexture = new Texture("texture/ui/slot.png");
    @Getter
    private Texture emptySlotTexture = new Texture("texture/ui/emptyslot.png");

    @Getter
    private int id;
    @Getter
    private int xInParent;
    @Getter
    private int yInParent;
    @Getter
    private Image imgBackground;
    @Getter
    private Image imgVirus;
    @Getter
    private Virus virus;

    public Slot(int id, int x, int y) {
        this.id = id;
        this.xInParent = x;
        this.yInParent = y;
        Slot.slotList.add(this);
    }

    public static Slot getById(int id) {
        for (Slot s : slotList)
            if (s.getId() == id)
                return s;
        return null;
    }

    public static void attributeVirus(int id, Virus virus) {
        Slot s;
        if ((s = getById(id)) != null)
            s.setVirus(virus);
    }

    public Slot setup() {
        this.setPosition(this.getXInParent(), this.getYInParent());
        this.addActor(imgBackground = new Image((virus != null) ? slotTexture : emptySlotTexture));
        return this;
    }

    public void setVirus(Virus virus) {
        this.virus = virus;
        imgBackground.remove();
        this.addActor(imgBackground = new Image((virus != null) ? slotTexture : emptySlotTexture));

        if (virus != null) {
            this.addActor(this.imgVirus = this.getVirus().getSmallImage());
            this.getVirus().getSmallImage().setPosition(20, 20);

            this.getImgVirus().addListener(new ClickListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    VirusUI.instance.showVirus(getVirus());
                    return true;
                }
            });

        }


    }

}
