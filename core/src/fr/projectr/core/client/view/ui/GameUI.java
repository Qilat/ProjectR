package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import fr.projectr.core.client.controller.listener.ChatListener;
import fr.projectr.core.client.controller.listener.OpenUIGeneral;
import lombok.Getter;

public class GameUI {

    @Getter
    private Stage stage;

    public GameUI() {
        this.stage = new Stage();

        this.registerUIComponents();

        this.registerListeners();
    }

    private void registerActor(Actor actor) {
        this.getStage().addActor(actor);
    }

    private void registerUIComponents() {
        this.registerActor(new ChatUI());
        this.registerActor(new InfosUI());
    }

    private void registerListener(EventListener listener) {
        this.getStage().addListener(listener);
    }

    private void registerListeners() {
        this.registerListener(new ChatListener.ChatFocusListener());
        this.registerListener(new ChatListener.ChatInputListener());
        this.registerListener(new OpenUIGeneral());
    }


}
