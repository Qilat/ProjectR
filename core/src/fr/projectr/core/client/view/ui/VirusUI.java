package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import fr.projectr.core.client.model.entity.virus.Virus;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 17/09/2017 for ProjectRClient.
 */
public class VirusUI extends WidgetGroup {

    public static VirusUI instance;
    @Getter
    @Setter
    private static boolean isOpen = false;

    private Texture bgdTexture;
    private Image background;


    private WidgetGroup virusInfoUI;

    private Texture virusInfoBackgroundTexture;
    private Image virusInfoBackgroundImg;

    private Texture cadrePhotoProfilTexture;
    private Image cadrePhotoProfilImg;

    private Image photoProfilImg;
    private Label name;

    public VirusUI() {
        instance = this;
    }

    public void setup() {
        this.bgdTexture = new Texture("texture/ui/virusui.png");
        this.background = new Image(this.bgdTexture);
        this.setPosition(this.getStage().getWidth() / 2 - this.background.getWidth() / 2, this.getStage().getHeight() / 2 - this.background.getHeight() / 2);
        this.addActor(this.background);

        this.addActor(Slot.S_0.setup());
        this.addActor(Slot.S_1.setup());
        this.addActor(Slot.S_2.setup());
        this.addActor(Slot.S_3.setup());
        this.addActor(Slot.S_4.setup());
        this.addActor(Slot.S_5.setup());

        //Slot.attributeVirus(0, new Virus("Yo", ProgramLang.C, VirusType.ASSEMBLER));

        this.virusInfoUI = new WidgetGroup();
        this.addActor(this.virusInfoUI);

        this.virusInfoBackgroundTexture = new Texture("texture/ui/virusinfo.png");
        this.virusInfoBackgroundImg = new Image(this.virusInfoBackgroundTexture);
        this.virusInfoUI.setPosition(250, 0);
        this.virusInfoUI.addActor(this.virusInfoBackgroundImg);

        loadVirusInfoUIBase();
    }

    public void showVirus(Virus virus) {
        this.name.setText(virus.getName());
        this.setPhotoProfilImg(virus.getProfilImage());
        this.virusInfoUI.addActor(photoProfilImg);
    }

    public void loadVirusInfoUIBase() {
        this.cadrePhotoProfilTexture = new Texture("texture/ui/cadrephotoprofil.png");
        this.cadrePhotoProfilImg = new Image(this.cadrePhotoProfilTexture);
        int padding = 20;
        this.cadrePhotoProfilImg.setPosition(padding, this.virusInfoBackgroundImg.getHeight() - (this.cadrePhotoProfilImg.getHeight() + padding));
        this.virusInfoUI.addActor(this.cadrePhotoProfilImg);

        this.name = new Label("Nom du virus", new Skin(Gdx.files.internal("texture/ui/uiskin.json")));
        this.name.setPosition(200, 350);
        this.virusInfoUI.addActor(this.name);

        this.setPhotoProfilImg(new Image(new Texture("texture/ui/photoprofil.png")));
        this.virusInfoUI.addActor(photoProfilImg);
    }

    private void setPhotoProfilImg(Image img) {
        int paddingPhoto = 20;
        this.photoProfilImg = img;
        this.photoProfilImg.setPosition(this.cadrePhotoProfilImg.getX() + paddingPhoto, this.cadrePhotoProfilImg.getY() + paddingPhoto);
    }

}
