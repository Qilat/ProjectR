package fr.projectr.core.client.view.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import fr.projectr.core.client.model.entity.EntityManager;

public class InfosUI extends WidgetGroup {

    private Label xPosition;
    private Label yPosition;

    InfosUI() {
        Skin skin = new Skin(Gdx.files.internal("texture/ui/uiskin.json"));
        this.xPosition = new Label("", skin, "default");
        this.yPosition = new Label("", skin, "default");

        this.xPosition.setPosition(20, Gdx.graphics.getHeight() - 10f);
        this.yPosition.setPosition(20, Gdx.graphics.getHeight() - 25f);

        this.addActor(this.xPosition);
        this.addActor(this.yPosition);
    }


    @Override
    public void act(float delta) {
        super.act(delta);

        this.xPosition.setText("x = " + EntityManager.get().getMainPlayer().getBody().getPosition().x);
        this.yPosition.setText("y = " + EntityManager.get().getMainPlayer().getBody().getPosition().y);

    }
}
