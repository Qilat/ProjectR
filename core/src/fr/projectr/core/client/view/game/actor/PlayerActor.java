package fr.projectr.core.client.view.game.actor;

import com.badlogic.gdx.graphics.Texture;
import fr.projectr.core.client.model.entity.human.Player;
import lombok.Getter;


/**
 * Created by Qilat on 02/09/2017.
 */
public class PlayerActor extends HumanEntityActor {

    @Getter
    private Player player;

    public PlayerActor(Player player) {
        super(player, new Texture("texture/player/player.png"), 9);
        this.player = player;
    }
}
