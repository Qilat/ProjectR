package fr.projectr.core.client.view.game.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import fr.projectr.core.client.model.animation.WalkingWay;
import fr.projectr.core.client.model.entity.human.HumanEntity;
import fr.projectr.core.client.view.game.GameScreen;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Theophile on 18/11/2018 for ProjectR.
 */
public class HumanEntityActor extends Group {

    private HumanEntity entity;

    private final int rows = 4;
    @Getter
    @Setter
    protected float frameDuration = 0.05f;

    private float time = 0f;
    @Getter
    @Setter
    private boolean moving = false;
    @Getter
    @Setter
    private boolean mustRender = true;
    private TextureRegion[][] regions;

    @Getter
    private Image currentImage;
    private Image img = new Image();

    @Getter
    @Setter
    private WalkingWay currentWay = WalkingWay.FRONT;
    @Getter
    private WalkingWay oldWay = WalkingWay.BACK;
    @Getter
    @Setter
    private Texture texture;
    @Getter
    @Setter
    private int colums;

    public HumanEntityActor(HumanEntity entity, Texture texture, int colums) {
        this.entity = entity;
        this.texture = texture;
        this.colums = colums;
        this.regions = new TextureRegion[rows][this.colums];
        this.reloadTexture();
    }

    public void reloadTexture() {
        for (int y = 0; y < rows; y++)
            for (int x = 0; x < colums; x++)
                regions[y][x] = new TextureRegion(this.texture, x * (this.texture.getWidth() / this.colums), y * (this.texture.getWidth() / this.colums), this.texture.getWidth() / this.colums, this.texture.getWidth() / this.colums);
    }

    private TextureRegion getFrame(float frameDuration) {
        float delta = Gdx.graphics.getDeltaTime();
        int key = 0;
        if (isMoving() || isMustRender()) {
            mustRender = false;
            time += delta;
            key = Math.round(time / frameDuration) % this.colums;
        }
        return regions[this.currentWay.getId()][key];

    }

    private Image getImage() {
        Image img = new Image(getFrame(this.getFrameDuration()));
        img.setScale(GameScreen.STANDARD_SCALE * 2f);
        return img;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (this.currentImage != null)
            this.currentImage.remove();
        this.currentImage = getImage();

        this.getStage().addActor(this.currentImage);

        this.currentImage.setPosition(this.entity.getBody().getPosition().x - 1f, this.entity.getBody().getPosition().y - 0.5f);
    }

}
