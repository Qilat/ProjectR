package fr.projectr.core.client.view.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.model.entity.Entity;
import fr.projectr.core.client.model.entity.human.Player;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public abstract class Map {

    public static final float TIME_STEP = 1 / 60f;
    public static final int VELOCITY_ITERATIONS = 6;
    public static final int POSITION_ITERATIONS = 2;
    @Getter
    private Stage stage;
    @Setter
    @Getter
    private TiledMap tiledMap;
    @Getter
    @Setter
    private World world;
    @Getter
    @Setter
    private ArrayList<Body> worldBorder = new ArrayList<>();
    @Setter
    @Getter
    private Player player;
    @Getter
    private ArrayList<Entity> entities = new ArrayList<>();
    @Getter
    private ArrayList<InputProcessor> inputProcessors = new ArrayList<>();
    private float accumulator = 0;

    public Map(Player player) {
        this.player = player;
        this.stage = new Stage(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        this.world = new World(new Vector2(0, 0), true);

        AssetManager assetManager = ProjectRClient.getCurrent().getAssetManager();
        this.loadAssets(assetManager);

        TiledMapTileLayer layer = (TiledMapTileLayer) this.tiledMap.getLayers().get(0);
        this.setupWorldBorder(layer.getWidth(), layer.getHeight());
        this.setNotAccessibleZone();

        this.loadEntities();
        entities.forEach(entity -> {
            System.out.println(entity.name);
            if (entity.getActor() != null) {
                System.out.println(true);
                Map.this.getStage().addActor(entity.getActor());
            }
        });

        this.player.setupBody(this.world, 10, 10);
        Map.this.getStage().addActor(this.player.getPlayerActor());

        this.loadListeners();
    }

    private void setNotAccessibleZone() {
        TiledMapTileLayer layer = (TiledMapTileLayer) this.tiledMap.getLayers().get("accessible");
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if (cell != null && cell.getTile() != null) {
                    boolean accessible = (boolean) cell.getTile().getProperties().get("accessible");
                    if (!accessible) {
                        this.createCubeBody(x, y);
                    }
                }
            }
        }
    }

    private void createCubeBody(float x, float y) {
        float length = 1f;

        BodyDef cubeBodyDef = new BodyDef();
        cubeBodyDef.type = BodyDef.BodyType.StaticBody;
        cubeBodyDef.position.set(new Vector2(x + 1 / 2f, y + 1 / 2f));
        Body cubeBody = world.createBody(cubeBodyDef);
        PolygonShape cubeBodyBox = new PolygonShape();
        cubeBodyBox.setAsBox(length / 2f, length / 2f);
        cubeBody.createFixture(cubeBodyBox, 100f);
        this.getWorldBorder().add(cubeBody);
        cubeBodyBox.dispose();
    }

    private void setupWorldBorder(float width, float height) {
        float borderWidth = 1f;

        BodyDef leftBodyDef = new BodyDef();
        leftBodyDef.type = BodyDef.BodyType.StaticBody;
        leftBodyDef.position.set(new Vector2(-borderWidth / 2f, height / 2f));
        Body leftBorder = world.createBody(leftBodyDef);
        PolygonShape leftBorderBox = new PolygonShape();
        leftBorderBox.setAsBox(borderWidth / 2f, height / 2f);
        leftBorder.createFixture(leftBorderBox, 100f);
        this.getWorldBorder().add(leftBorder);
        leftBorderBox.dispose();

        BodyDef rightBodyDef = new BodyDef();
        rightBodyDef.type = BodyDef.BodyType.StaticBody;
        rightBodyDef.position.set(new Vector2(borderWidth / 2f + width, height / 2f));
        Body rightBorder = world.createBody(rightBodyDef);
        PolygonShape rightBorderBox = new PolygonShape();
        rightBorderBox.setAsBox(borderWidth / 2f, height / 2f);
        rightBorder.createFixture(rightBorderBox, 0.0f);
        this.getWorldBorder().add(rightBorder);
        rightBorderBox.dispose();

        BodyDef upBodyDef = new BodyDef();
        upBodyDef.type = BodyDef.BodyType.StaticBody;
        upBodyDef.position.set(new Vector2(width / 2f, height + borderWidth / 2f));
        Body upBorder = world.createBody(upBodyDef);
        PolygonShape upBorderBox = new PolygonShape();
        upBorderBox.setAsBox(width / 2, borderWidth / 2f);
        upBorder.createFixture(upBorderBox, 0.0f);
        this.getWorldBorder().add(upBorder);
        upBorderBox.dispose();

        BodyDef downBodyDef = new BodyDef();
        downBodyDef.type = BodyDef.BodyType.StaticBody;
        downBodyDef.position.set(new Vector2(width / 2f, -borderWidth / 2f));
        Body downBorder = world.createBody(downBodyDef);
        PolygonShape downBorderBox = new PolygonShape();
        downBorderBox.setAsBox(width / 2, borderWidth / 2f);
        downBorder.createFixture(downBorderBox, 0.0f);
        this.getWorldBorder().add(downBorder);
        downBorderBox.dispose();
    }

    public void doPhysicsStep(float deltaTime) {
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        while (accumulator >= TIME_STEP) {
            world.step(TIME_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
            accumulator -= TIME_STEP;
        }
    }

    public abstract void loadAssets(AssetManager assetManager);

    void registerEntity(Entity entity) {
        this.getEntities().add(entity);
    }

    public abstract void loadEntities();

    public abstract void loadListeners();
}