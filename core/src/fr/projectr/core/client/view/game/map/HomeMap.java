package fr.projectr.core.client.view.game.map;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import fr.projectr.core.client.controller.listener.PlayerMoveListener;
import fr.projectr.core.client.model.animation.WalkingWay;
import fr.projectr.core.client.model.entity.EntityManager;
import fr.projectr.core.client.model.entity.NPCDesc;
import fr.projectr.core.client.model.entity.human.NPCHumanEntity;
import fr.projectr.core.client.model.entity.human.Player;


public class HomeMap extends Map {

    private static final String MAP_PATH = "texture/map/home/home.tmx";

    public HomeMap(Player player) {
        super(player);

    }

    @Override
    public void loadAssets(AssetManager assetManager) {
        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        assetManager.load(MAP_PATH, TiledMap.class);
        assetManager.finishLoading();

        this.setTiledMap(assetManager.get(MAP_PATH));
    }

    @Override
    public void loadEntities() {
        this.registerEntity(this.getPlayer());

        NPCDesc samDesc = new NPCDesc("Sam", new Texture("texture/player/player.png"), 9, new Vector2(32.5f, 17f), WalkingWay.FRONT);
        NPCHumanEntity sam = EntityManager.get().createNPC(samDesc);
        sam.setupBody(this.getWorld(), sam.getDesc().getPosition().x, sam.getDesc().getPosition().y);
        this.registerEntity(sam);
    }

    @Override
    public void loadListeners() {
        this.getStage().addListener(new PlayerMoveListener(this.getPlayer()));
    }
}
