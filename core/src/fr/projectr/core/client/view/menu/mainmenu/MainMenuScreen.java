package fr.projectr.core.client.view.menu.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import fr.projectr.core.client.view.menu.mainmenu.background.Background;
import fr.projectr.core.client.view.menu.mainmenu.ui.MainMenuUI;
import lombok.Getter;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class MainMenuScreen implements Screen {

    @Getter
    private Camera camera;
    @Getter
    private Background background;
    @Getter
    private MainMenuUI mainMenuUI;

    public MainMenuScreen() {
        this.background = new Background();
        this.mainMenuUI = new MainMenuUI();

        this.camera = this.background.getStage().getCamera();

        Gdx.input.setInputProcessor(this.mainMenuUI.getStage());
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        this.background.getStage().act();
        this.background.getStage().draw();

        this.mainMenuUI.getStage().act();
        this.mainMenuUI.getStage().draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
