package fr.projectr.core.client.view.menu.mainmenu.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class MainMenuButtonUI extends TextButton {

    public MainMenuButtonUI(String text, float width, float height, float x, float y) {
        super(text, new Skin(Gdx.files.internal("texture/ui/uiskin.json")));
        this.setWidth(width);
        this.setHeight(height);
        this.setPosition(x - width / 2, y - height / 2);
    }
}
