package fr.projectr.core.client.view.menu.mainmenu.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import fr.projectr.core.client.view.ScreenManager;
import lombok.Getter;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class MainMenuUI {

    @Getter
    private Stage stage;

    public MainMenuUI() {
        this.stage = new Stage();
        this.registerUIComponents();
        this.registerListeners();
    }

    private void registerActor(Actor actor) {
        this.getStage().addActor(actor);
    }

    private void registerUIComponents() {

        float space = 30f;
        float buttonWidth = 200f;
        float buttonHeight = 50f;
        float globalUpDelta = 175f;

        MainMenuButtonUI playButton = new MainMenuButtonUI("Jouer", buttonWidth, buttonHeight, this.stage.getCamera().viewportWidth / 2,
                this.stage.getCamera().viewportHeight / 2 - 1 * (buttonHeight + space) + globalUpDelta);
        playButton.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        ScreenManager.get().switchToScreen(ScreenManager.ScreenType.GAME);
                    }
                }
        );

        this.registerActor(playButton);

        MainMenuButtonUI optionsButton = new MainMenuButtonUI("Options", buttonWidth, buttonHeight, this.stage.getCamera().viewportWidth / 2,
                this.stage.getCamera().viewportHeight / 2 - 2 * (buttonHeight + space) + globalUpDelta);
        optionsButton.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        ScreenManager.get().switchToScreen(ScreenManager.ScreenType.OPTIONS);
                    }
                }
        );

        this.registerActor(optionsButton);

        MainMenuButtonUI quitButton = new MainMenuButtonUI("Quitter", buttonWidth, buttonHeight, this.stage.getCamera().viewportWidth / 2,
                this.stage.getCamera().viewportHeight / 2 - 3 * (buttonHeight + space) + globalUpDelta);
        quitButton.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Gdx.app.exit();
                    }
                }
        );

        this.registerActor(quitButton);
    }

    private void registerListener(EventListener listener) {
        this.getStage().addListener(listener);
    }

    private void registerListeners() {
        // this.registerListener(new ChatListener.ChatFocusListener());
    }

}
