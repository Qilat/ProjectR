package fr.projectr.core.client.controller.listener;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import fr.projectr.core.client.view.ui.ChatUI;
import fr.projectr.core.client.view.ui.VirusUI;

/**
 * Created by Qilat on 17/09/2017 for ProjectRClient.
 */
public class OpenUIGeneral extends InputListener {
    public boolean keyDown(InputEvent event, int keycode) {
        switch (keycode) {
            case Input.Keys.T:
            case Input.Keys.I:
                if (!ChatUI.isTextBarFocused())
                    if (VirusUI.isOpen()) {
                        //ProjectRClient.getCurrent().getCurrentScreen().closeVirusUI();
                    } else {
                        //ProjectRClient.getCurrent().getCurrentScreen().openVirusUI();
                    }
                break;
        }
        return false;
    }

}

/**
 * public void openVirusUI() {
 * this.uiStage.addActor(virusUI = new VirusUI());
 * virusUI.setup();
 * VirusUI.setOpen(true);
 * }
 * <p>
 * public void closeVirusUI() {
 * if (this.virusUI != null)
 * this.virusUI.remove();
 * this.virusUI = null;
 * VirusUI.setOpen(false);
 * }
 */