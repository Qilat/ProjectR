package fr.projectr.core.client.controller.listener;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import fr.projectr.core.client.model.entity.human.Player;
import lombok.Getter;

public class PlayerMoveListener extends InputListener {

    @Getter
    private Player player;


    public PlayerMoveListener(Player player) {
        this.player = player;
    }

    @Override
    public boolean keyUp(InputEvent event, int keycode) {

        switch (keycode) {
            case Input.Keys.LEFT:
            case Input.Keys.A:
            case Input.Keys.RIGHT:
            case Input.Keys.D:
                this.player.getBody().setLinearVelocity(0, this.player.getBody().getLinearVelocity().y);
                break;
            case Input.Keys.DOWN:
            case Input.Keys.S:
            case Input.Keys.UP:
            case Input.Keys.W:
                this.player.getBody().setLinearVelocity(this.player.getBody().getLinearVelocity().x, 0);
                break;
        }

        return false;
    }
}
