package fr.projectr.core.client.controller.listener;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.FocusListener;
import fr.projectr.core.client.view.ui.ChatUI;
import fr.projectr.core.client.view.ui.VirusUI;

/**
 * Created by Qilat on 18/09/2017 for ProjectRClient.
 */
public class ChatListener extends InputListener {
    public static class ChatInputListener extends InputListener {
        @Override
        public boolean keyDown(InputEvent event, int keycode) {
            if (keycode == Input.Keys.ENTER) {
                if (!VirusUI.isOpen()) {
                    if (ChatUI.isTextBarFocused()) {
                        if (!ChatUI.getTextField().getText().equals("")) {
                            ChatUI.sendMessage(ChatUI.getTextField().getText());
                        }
                        ChatUI.getInstance().getStage().unfocus(ChatUI.getTextField());
                    } else {
                        ChatUI.getInstance().getStage().setKeyboardFocus(ChatUI.getTextField());
                    }
                }
            }
            return false;
        }
    }

    public static class ChatFocusListener extends FocusListener {
        public void keyboardFocusChanged(FocusEvent event, Actor actor, boolean focused) {
            if (actor.equals(ChatUI.getTextField())) {
                if (VirusUI.isOpen() && focused) {
                    ChatUI.getInstance().getStage().unfocus(ChatUI.getTextField());
                } else {
                    ChatUI.setTextBarFocused(focused);
                }
            }
        }
    }
}
