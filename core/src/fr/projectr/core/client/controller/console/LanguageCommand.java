package fr.projectr.core.client.controller.console;

import fr.projectr.api.utils.lang.Lang;
import fr.projectr.api.utils.lang.LangType;
import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.model.command.Command;
import fr.projectr.core.client.model.entity.CommandSender;

import java.util.HashMap;

/**
 * Created by Qilat on 14/08/2017.
 */
public class LanguageCommand extends Command {

    public LanguageCommand() {
        super("language", "lang", "langue");
    }

    @Override
    public boolean execute(CommandSender sender, Command cmd, String[] input) {
        if (input.length > 1) {
            String langVal = input[1];
            LangType newType = LangType.getByFileName(langVal);
            if (newType == null)
                try {
                    newType = LangType.valueOf(langVal);
                } catch (IllegalArgumentException e) {
                    return false;
                }

            if (Lang.CUSTOM_LANG_ID != newType) {
                Lang.CUSTOM_LANG_ID = newType;
                Lang.reload();

                HashMap<String, String> metadata = new HashMap<>();
                metadata.put("lang", Lang.get(Lang.getCurrentLangType().toString(), new HashMap<>()));
                ProjectRClient.getLogger().info(Lang.get("set_lang", metadata));
            } else {
                ProjectRClient.getLogger().info(Lang.get("lang_already_set", new HashMap<>()));
            }
            return true;

        }
        return false;
    }
}
