package fr.projectr.core.client.utils.sql;

import fr.projectr.api.utils.SaveManager;
import fr.projectr.core.client.ProjectRClient;

import java.sql.*;

/**
 * Created by Theophile on 14/11/2018 for ProjectRClient.
 */
public class SQLDriver {

    private static final String CONNECTION_PREFIX = "jdbc:sqlite:";

    void connect() {
        Connection connection = null;
        try {
            String url = "jdbc:sqlite:" + SaveManager.get().getSaveDBFile().getAbsolutePath();
            connection = DriverManager.getConnection(url);

            ProjectRClient.getLogger().info("Connection to SQLite has been established.");
        } catch (SQLException e) {
            ProjectRClient.getLogger().error(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                ProjectRClient.getLogger().error(ex.getMessage());
            }
        }
    }

    void createDatabase(String name) {
        String url = CONNECTION_PREFIX + SaveManager.get().getParentFolder().getAbsolutePath() + "/" + name;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createNewTable(String dbName, String tableName) {
        String url = CONNECTION_PREFIX + SaveManager.get().getParentFolder().getAbsolutePath() + "/" + dbName;

        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	capacity real\n"
                + ");";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
