package fr.projectr.core.client.utils.sql;

import fr.projectr.api.utils.IModule;
import lombok.Getter;

/**
 * Created by Theophile on 14/11/2018 for ProjectRClient.
 * Must be load after SaveManager
 */
public class SQLManager implements IModule {

    private static SQLManager manager;

    public static SQLManager get() {
        return manager;
    }

    @Getter
    private SQLDriver driver;


    @Override
    public boolean init() {

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        this.driver = new SQLDriver();
        this.driver.connect();

        this.driver.createDatabase("entity");
        this.driver.createNewTable("entity", "general");
        this.driver.createNewTable("entity", "property1");
        this.driver.createNewTable("entity", "property2");

        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }
}
