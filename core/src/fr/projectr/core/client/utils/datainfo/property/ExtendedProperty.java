package fr.projectr.core.client.utils.datainfo.property;

import fr.projectr.core.client.utils.datainfo.Info;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Theophile on 02/08/2018 for webapi.
 */
public abstract class ExtendedProperty<T extends Info> {

    @Getter
    @Setter
    T info;
    @Getter
    private String key;

    /**
     * Called before field update
     *
     * @param info userInfo
     */
    public void init(T info) {
        this.info = info;
        this.key = this.getPropertyTag();
    }

    /**
     * Called before field load
     */
    abstract public void preLoad();

    /**
     * Called after field load
     */
    abstract public void postLoad();

    /**
     * Called before field save
     */
    abstract public void preSave();

    /**
     * Called after field save
     */
    abstract public void postSave();

    abstract public String getPropertyTag();

}