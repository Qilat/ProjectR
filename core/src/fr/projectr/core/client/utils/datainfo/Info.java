/*
 * Copyright 404Team (c) 2018. For all uses ask 404Team for approuval before.
 */
package fr.projectr.core.client.utils.datainfo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.utils.UtilsReflect;
import fr.projectr.core.client.utils.datainfo.property.ExtendedProperty;
import fr.projectr.core.client.utils.datainfo.property.SavedItem;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Helliot on 19/08/2018 for webapi.
 */
public abstract class Info<T extends ExtendedProperty> {

    @Getter(AccessLevel.PRIVATE)
    private HashMap<String, T> propertiesMap = new HashMap<>();

    @Getter
    private UUID uuid;

    protected Info(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Add a property to this User
     *
     * @param property property to add
     */
    public void addProperty(T property) {
        if (this.getPropertiesMap().get(property.getPropertyTag()) != null) {
            ProjectRClient.getLogger().warn("Unable to register two properties with the same propertytag : " + property.getPropertyTag());
        } else {
            this.getPropertiesMap().put(property.getPropertyTag(), property);
        }
    }

    /**
     * Add many properties to this User
     *
     * @param propertiesTag properties tag to add
     */
    public abstract void addProperties(Collection<String> propertiesTag);

    /**
     * Remove a property to this User
     *
     * @param propertyTag propertyTag of the property to remove
     */
    public Info removeProperty(String propertyTag) {
        if (getPropertiesMap().get(propertyTag) == null) {
            ProjectRClient.getLogger().warn("Unable to remove a property doesn't exist in playerActor's data map.");
        } else {
            getPropertiesMap().remove(propertyTag);
        }
        return this;
    }

    @SuppressWarnings("unchecked")
    public <U extends T> U getProperty(String propertyTag) {
        return (U) this.getPropertiesMap().get(propertyTag);
    }

    private void initOneProperty(T property) {
        property.init(this);
    }

    public void initAllProperties() {
        propertiesMap.forEach((s, property) -> Info.this.initOneProperty(property));
    }

    protected String wrapPropertyInJSON(T property) {
        HashMap<String, String> propertyData = new HashMap<>();

        Iterable<Field> fields = UtilsReflect.getFieldsUpTo(property.getClass(), ExtendedProperty.class);
        fields.forEach(field -> {
            SavedItem desc = field.getAnnotation(SavedItem.class);
            if (desc != null) {
                try {
                    boolean oldAccessible = field.isAccessible();
                    field.setAccessible(true);
                    propertyData.put(desc.value(), new ObjectMapper().writeValueAsString(field.get(property)));
                    field.setAccessible(oldAccessible);
                } catch (IllegalAccessException | JsonProcessingException e) {
                    ProjectRClient.getLogger().error(ExceptionUtils.getStackTrace(e));
                }
            }
        });

        try {
            return new ObjectMapper().writeValueAsString(propertyData);
        } catch (JsonProcessingException e) {
            ProjectRClient.getLogger().error(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    /**
     * Save one property in redis.
     *
     * @param property property to save
     */
    protected abstract void saveOneProperty(T property);

    /**
     * Save all properties in redis
     */
    public void saveAllProperties() {
        propertiesMap.forEach((s, property) -> Info.this.saveOneProperty(property));
    }


    @SuppressWarnings("unchecked")
    protected void unwrapPropertyFromJSON(T property, String json) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
            };
            Map<String, String> propertyDataMap = mapper.readValue(json, typeRef);

            Iterable<Field> fields = UtilsReflect.getFieldsUpTo(property.getClass(), ExtendedProperty.class);
            for (Field field : fields) {
                SavedItem desc = field.getAnnotation(SavedItem.class);
                if (desc != null) {
                    String valueStr = propertyDataMap.get(desc.value());
                    if (valueStr != null) {
                        Object fieldData;
                        if (Collection.class.isAssignableFrom(field.getType()) && desc.firstGenericClass() != Integer.class) {
                            fieldData = UtilsReflect.getCollection(valueStr, (Class<? extends Collection>) field.getType(), desc.firstGenericClass());

                        } else if (Map.class.isAssignableFrom(field.getType()) && (desc.firstGenericClass() != Integer.class || desc.secondGenericClass() != Integer.class)) {
                            fieldData = UtilsReflect.getMap(valueStr, (Class<? extends Map>) field.getType(), desc.firstGenericClass(), desc.secondGenericClass());

                        } else {
                            fieldData = mapper.readValue(valueStr, field.getType());
                        }

                        try {
                            boolean oldAccessible = field.isAccessible();
                            field.setAccessible(true);
                            if (fieldData != null)
                                field.set(property, fieldData);
                            field.setAccessible(oldAccessible);
                        } catch (IllegalAccessException e) {
                            ProjectRClient.getLogger().error(ExceptionUtils.getStackTrace(e));
                        }
                    }
                }
            }
        } catch (IOException e) {
            ProjectRClient.getLogger().error(ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * Load one property from redis.
     *
     * @param property property to load
     */
    protected abstract void loadOneProperty(T property, String propertyData);

    /**
     * Load all properties from redis
     */
    public void loadAllProperties() {
        propertiesMap.forEach((s, property) -> Info.this.loadOneProperty(property, null));
    }
}
