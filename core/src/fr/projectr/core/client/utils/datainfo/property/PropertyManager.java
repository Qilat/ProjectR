package fr.projectr.core.client.utils.datainfo.property;

import fr.projectr.core.client.ProjectRClient;
import fr.projectr.core.client.utils.datainfo.Info;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Theophile on 01/11/2018 for webapi.
 */
public abstract class PropertyManager<T extends Info, U extends ExtendedProperty<T>> {

    private HashMap<String, Class<? extends U>> propertiesMap = new HashMap<>();

    public void registerProperty(String propertyTag, Class<? extends U> propertyClass) {
        propertiesMap.put(propertyTag, propertyClass);
    }

    public void unregisterProperty(String propertyTag) {
        propertiesMap.remove(propertyTag);
    }

    @Deprecated
    public void flushPropertiesMap() {
        propertiesMap.clear();
    }

    public Class<? extends U> getPropertyClass(String propertyTag) {
        return propertiesMap.get(propertyTag);
    }

    public String getPropertyTag(Class<? extends U> propertyClass) {
        try {
            return propertyClass.newInstance().getPropertyTag();
        } catch (InstantiationException | IllegalAccessException e) {
            ProjectRClient.getLogger().error(ExceptionUtils.getStackTrace(e));
        }
        return null;
    }

    public Collection<String> getAllPropertiesTag() {
        return propertiesMap.keySet();
    }
}
