package fr.projectr.core.client;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.physics.box2d.Box2D;
import fr.projectr.api.API;
import fr.projectr.api.utils.IModule;
import fr.projectr.api.utils.SaveManager;
import fr.projectr.api.utils.config.Configuration;
import fr.projectr.api.utils.lang.Lang;
import fr.projectr.core.client.model.chat.ChatManager;
import fr.projectr.core.client.model.command.CommandManager;
import fr.projectr.core.client.model.console.ConsoleManager;
import fr.projectr.core.client.model.entity.EntityManager;
import fr.projectr.core.client.view.ScreenManager;
import fr.projectr.core.client.view.ui.UIManager;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import java.io.File;
import java.util.ArrayList;

public class ProjectRClient extends Game {

    @Getter
    private static ProjectRClient current;

    public static ProjectRClient get() {
        return current;
    }

    @Getter
    private static Logger logger = Logger.getLogger(ProjectRClient.class);
    @Getter
    private static API api;
    @Setter(AccessLevel.PACKAGE)
    @Getter
    private Scheduler scheduler;

    @Getter
    private static File configFile;
    @Getter
    private static Configuration config;

    @Getter
    private AssetManager assetManager;


    @Getter
    private Screen currentScreen;

    private ArrayList<IModule> modules = new ArrayList<>();

    private void registerModule(IModule module) {
        modules.add(module);
    }

    private void registerModules() {
        registerModule(new SaveManager());
        //registerModule(new SQLManager());
        registerModule(new EntityManager());
        registerModule(new ConsoleManager(this.getScheduler()));
        registerModule(new UIManager());
        registerModule(new CommandManager());
        registerModule(new ChatManager());
    }

    @Override
    public void create() {
        ProjectRClient.current = this;

        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
        ProjectRClient.getLogger().info("Start of ProjectRClient");

        ProjectRClient.getLogger().info("Loading API");
        ProjectRClient.api = new API(getLogger(), Gdx.files.getLocalStoragePath());

        ProjectRClient.getLogger().info("Loading of Quart Scheduler");
        try {
            this.setScheduler(StdSchedulerFactory.getDefaultScheduler());
            this.getScheduler().start();
        } catch (SchedulerException e) {
            e.printStackTrace();
            Gdx.app.exit();
            return;
        }

        ProjectRClient.getLogger().info("Loading Config");
        /*try {
            //configFile = Gdx.files.internal("config.yml").file();



            //config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            //Lang.CUSTOM_LANG_ID = LangType.getByFileName((String) config.get("lang"));
        } catch (IOException e) {
            e.printStackTrace();
            Gdx.app.exit();
            return;
        }*/

        ProjectRClient.getLogger().info("Loading AssetManager");
        this.assetManager = new AssetManager();

        ProjectRClient.getLogger().info("Loading Module");
        this.registerModules();

        for (IModule module : modules) {
            ProjectRClient.getLogger().info("[Module] " + module.getClass().getSimpleName() + " loading...");
            if (!module.init()) {
                ProjectRClient.getLogger().error("[Module] Cannot load " + module.getClass().getSimpleName() + " module. Shutdown...");
                Gdx.app.exit();
            }
        }
        for (IModule module : modules) {
            ProjectRClient.getLogger().info("[Module] " + module.getClass().getSimpleName() + " registering commands...");
            module.registerCommands();
        }

        ProjectRClient.getLogger().info("Loading Lang File...");
        Lang.reload();

        ProjectRClient.getLogger().info("Loading Box2d");
        Box2D.init();

        ProjectRClient.getLogger().info("ProjectRClient ready.");

        ScreenManager.get().switchToScreen(ScreenManager.ScreenType.MAINMENU);
    }

    public void render() {
        super.render();

    }

    public void dispose() {
        super.dispose();

        for (IModule module : modules) {
            ProjectRClient.getLogger().info("[Module] " + module.getClass().getSimpleName() + " stoping...");
            module.stop();
        }

        try {
            scheduler.shutdown(false);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setScreen(Screen screen) {
        super.setScreen(screen);
        this.currentScreen = screen;
    }
}