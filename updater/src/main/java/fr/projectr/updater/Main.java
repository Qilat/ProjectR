package fr.projectr.updater;

import fr.projectr.api.API;
import fr.projectr.api.utils.Save;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.net.URL;


/**
 * Created by Qilat on 07/10/2017 for ProjectR.
 */
public class Main extends Application {

    private static API api;

    public static void main(String[] args){
        api = new API(Logger.getLogger(Main.class), Save.getParentFolder());

        Main.launch(Main.class, new String[]{});
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL fxmlURL = getClass().getResource("../../../../resources/updater.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);

        Pane root = fxmlLoader.load();

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);

        primaryStage.show();
    }


    /*

    Déroulement
    0) Comparage des deux versions.json du launcher
    1) copie du version.json du launcher depuis internet
    2) prise du json load
    3) Téléchargment si version ultérieur dispo
    4) Lancement du launcher

     */


}
