package fr.projectr.server.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import fr.projectr.api.network.ClientInfo;
import fr.projectr.api.network.listener.ReceivedListener;
import fr.projectr.api.network.packet.Packet;
import fr.projectr.api.utils.IModule;
import lombok.Getter;

import java.io.IOException;
import java.util.HashMap;

public class NetworkServerManager implements IModule {

    private static NetworkServerManager manager;

    public static NetworkServerManager get() {
        return manager;
    }

    @Getter
    private Server kryoServer;
    @Getter
    private HashMap<Integer, ClientInfo> clientInfos = new HashMap<>();

    @Override
    public boolean init() {
        manager = this;
        this.kryoServer = new Server();

        kryoServer.start();
        Kryo kryo = kryoServer.getKryo();
        kryo.register(String.class);
        try {
            kryoServer.bind(54555, 54777);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.registerKryoListeners();

        return true;
    }

    @Override
    public void registerCommands() {

    }

    @Override
    public void stop() {

    }

    public void registerKryoListener(Listener listener) {
        this.kryoServer.addListener(listener);
    }

    public void registerKryoListeners() {
        this.registerKryoListener(new ConnectionListener());
        this.registerKryoListener(new ReceivedListener());
    }

    public void registerPackets() {

    }

    public void sendPacketToOne(int connectionId, Packet packet) {
        this.kryoServer.sendToTCP(connectionId, packet.toString());
    }

    public void sendPacketToAll(Packet packet) {
        this.kryoServer.sendToAllTCP(packet.toString());
    }

}
