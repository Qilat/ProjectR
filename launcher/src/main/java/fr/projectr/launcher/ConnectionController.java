package fr.projectr.launcher;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import jdk.nashorn.internal.objects.annotations.Getter;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Qilat on 29/09/2017 for ProjectR.
 */
public class ConnectionController implements Initializable {

    static ConnectionController controller;

    @FXML
    private Button connectBtt;

    @FXML
    private TextField pseudoTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private ProgressBar dlProgressBar;

    @FXML
    private Label progressbarLbl;

    @FXML
    private Button websiteBtt;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        controller = this;
        dlProgressBar.setProgress(0);
    }

    @FXML
    public void onClickConnectBtt(){
        System.out.println("Trying to connect with : " +pseudoTextField.getText() + " : " + passwordField.getText());
        AuthManager.requestAuth(pseudoTextField.getText(), passwordField.getText());
        //Launcher.launchGame(new String[]{});
        Updater.get().update();
    }

    public static void updateProgressBar(double value, double max){
        controller.progressbarLbl.setVisible(true);
        controller.dlProgressBar.setVisible(true);
        controller.progressbarLbl.setText((int)value + "/" + (int)max);
        controller.dlProgressBar.setProgress(value/max);
    }

    int i = 0;
    @FXML
    public void onClickWebsiteBtt(){
        updateProgressBar(i++, 10);
    }

    public static ProgressBar getDlProgressBar() {
        return controller.dlProgressBar;
    }

}