package fr.projectr.launcher;

import fr.projectr.api.net.AuthRequest;
import fr.projectr.api.net.AuthResponse;

import java.util.LinkedHashMap;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class AuthManager {
    public static AuthManager instance;
    public static LinkedHashMap<AuthRequest, AuthResponse> authList = new LinkedHashMap<>();

    public AuthManager(){
        if(instance== null)
            instance = this;
    }


    public static void requestAuth(String name, String password){
        AuthRequest authRequest = new AuthRequest(name, password);
        authList.put(authRequest, null);
        KryoManager
                .getManager()
                .sendAuthRequest(
                        authRequest);
    }
}
