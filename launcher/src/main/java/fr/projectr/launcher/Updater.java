package fr.projectr.launcher;

import fr.projectr.api.API;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import lombok.Getter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class Updater {

    @Getter
    private static Updater instance = new Updater();

    private static String versionJsonName = "versions.json";

    URL dlURL;

    public Task updateProgressBar;

    public int byteDownloaded;

    int fileSize;

    private Updater(){
        ConnectionController.getDlProgressBar().setProgress(0);
        ConnectionController.getDlProgressBar().setVisible(true);
    }

    public void update(){
        File versionJson = new File(API.getDefaultPath() + "/" + versionJsonName );

        try {
            System.out.println("Copy");

            dlURL = new URL("http://qilat.fr/projectr/desktop/desktop.jar");
            fileSize = getFileSize(dlURL);
            BufferedInputStream bis = new BufferedInputStream(dlURL.openStream());
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(API.getDefaultPath() + "/" + "desktop.jar"));

            int fileSize = getFileSize(dlURL);

            byte[] buf = new byte[8];
            byteDownloaded = 0;

            updateProgressBar = createWorker();

            ConnectionController.getDlProgressBar().progressProperty().unbind();
            ConnectionController.getDlProgressBar().progressProperty().bind(updateProgressBar.progressProperty());

            updateProgressBar.messageProperty().addListener(new ChangeListener<String>() {
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    System.out.println("test");
                }
            });


            new Thread(updateProgressBar).start();

            while(bis.read(buf) != -1){
                bos.write(buf);
                byteDownloaded+=8;
            }


            bis.close();
            bos.close();

            System.out.println("bite");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int getFileSize(URL url) {
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("HEAD");
            conn.getInputStream();
            return conn.getContentLength();
        } catch (IOException e) {
            return -1;
        } finally {
            conn.disconnect();
        }
    }

    private Task createWorker(){
        return new Task() {
            @Override
            protected Object call() throws Exception {
                while (byteDownloaded < fileSize) {
                    //Thread.sleep(500);
                    updateMessage("500 milliseconds");
                    updateProgress(byteDownloaded, fileSize);
                    System.out.println("test");
                }
                return true;
            }
        };
    }

    public static Updater get() {
        return instance;
    }
}
