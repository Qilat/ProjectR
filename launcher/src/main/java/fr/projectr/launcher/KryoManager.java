package fr.projectr.launcher;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import fr.projectr.api.net.AuthRequest;
import fr.projectr.api.net.AuthResponse;
import lombok.Getter;

import java.io.IOException;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class KryoManager {
    @Getter
    private static KryoManager manager = null;
    @Getter
    public Client client ;

    public KryoManager(){
            manager = this;
            this.start();

    }

    private void start(){
        client = new Client();
        client.start();

        Kryo kryo = client.getKryo();
        kryo.register(AuthRequest.class);
        kryo.register(AuthResponse.class);
        kryo.register(AuthResponse.Type.class);

        try {
            client.connect(5000, "localhost", 54555, 54777);
            System.out.println("Connected");
        } catch (IOException e) {
            e.printStackTrace();
        }

        client.addListener(new Listener(){
            public void received(Connection connection, Object object) {
                if(object instanceof AuthResponse){
                    AuthResponse response = (AuthResponse) object;

                    System.out.println(response.getName() + " : " + response.getType());


                }
            }
        });
    }

    public void sendAuthRequest(AuthRequest request){
        client.sendTCP(request);
    }
}
