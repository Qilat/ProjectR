package fr.projectr.api.utils.lang;

import fr.projectr.api.API;
import fr.projectr.api.utils.config.Configuration;
import fr.projectr.api.utils.config.ConfigurationProvider;
import fr.projectr.api.utils.config.YamlConfiguration;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Qilat on 13/08/2017.
 */
public class Lang {
    public static final LangType DEFAULT_LANG_ID = LangType.en_US;
    public static LangType CUSTOM_LANG_ID = null;

    private static Configuration defaultLang = null;
    private static Configuration customLang = null;

    private static HashMap<String, String> defaultLangList = new HashMap<>();
    private static HashMap<String, String> customLangList = new HashMap<>();

    @Getter
    private static HashMap<String, String> GENERIC_VALUE = new HashMap<>();

    /**
     * Load the language file.
     */
    private static void load() {
        if (defaultLang == null) {
            try {
                File file = new File(API.getDefaultPath() + "/lang/" + DEFAULT_LANG_ID.getFileName() + ".yml");
                if(!new File(file.getParent()).exists()) {
                    new File(file.getParent()).mkdir();

                }
                if(!file.exists())
                    file.createNewFile();

                defaultLang = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
                for (String key : defaultLang.getKeys()) {
                    defaultLangList.put(key, (String) defaultLang.get(key));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (DEFAULT_LANG_ID != CUSTOM_LANG_ID
                && customLang == null
                && CUSTOM_LANG_ID != null) {
            try {
                File file = new File(API.getDefaultPath() + "/lang/" + CUSTOM_LANG_ID.getFileName() + ".yml");
                if(!new File(file.getParent()).exists()) {
                    new File(file.getParent()).mkdir();
                }
                if(!file.exists())
                    file.createNewFile();
                customLang = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);

                for (String key : customLang.getKeys()) {
                    customLangList.put(key, (String) customLang.get(key));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        loadGeneric();
    }

    /**
     * Reload the language file when changing language during process.
     */
    public static void reload() {
        customLang = null;
        load();
    }

    /**
     * Get the translation of a value.
     *
     * @param path Path of the translated value.
     * @return Local value of the path. Default in en_US if not found in custom language.
     */
    public static String get(String path, HashMap<String, String> metadata) {
        String returnVal = defaultLangList.get(path);

        if (customLang != null)
            if ((returnVal = customLangList.get(path)) == null)
                returnVal = defaultLangList.get(path);

        loadGeneric();
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            returnVal = returnVal.replace("%" + entry.getKey() + "%", entry.getValue());
        }

        return returnVal;
    }
    /**
     * Get the translation of a value.
     *
     * @param path Path of the translated value.
     * @return Local value of the path. Default in en_US if not found in custom language.
     */
    public static String get(String path) {
        return get(path, new HashMap<>());
    }

    public static void changeLang(LangType newLang) {
        CUSTOM_LANG_ID = newLang;
        //TODO Check save customLanguage in config.yml (maybe copy config.yml in AppData ?)
        API.getConfig().set("lang", CUSTOM_LANG_ID.getFileName());
        reload();
    }

    public static void loadGeneric() {
        //GENERIC_VALUE.put("input", Console.input);
        //GENERIC_VALUE.put("lang", (Lang.CUSTOM_LANG_ID != null ? Lang.CUSTOM_LANG_ID.getLangName() : Lang.DEFAULT_LANG_ID.getLangName()));
    }

    public static LangType getCurrentLangType(){
        return CUSTOM_LANG_ID != null ? CUSTOM_LANG_ID : DEFAULT_LANG_ID;
    }

}
