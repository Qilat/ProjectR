package fr.projectr.api.utils.enums;

import fr.projectr.api.utils.lang.Lang;

/**
 * Created by Qilat on 14/08/2017.
 */
public enum StopReason {
    CMD_PLAYER(Lang.get("stop_reason_player_cmd")),
    CMD_CONSOLE(Lang.get("stop_reason_console_cmd")),
    ERROR("because of a fatal error.");

    String string;

    StopReason(String string) {
        this.setString(string);
    }

    public String getString() {
        return string;
    }

    private void setString(String str) {
        this.string = str;
    }
}
