package fr.projectr.api.utils;

/**
 * Created by Theophile on 09/11/2018 for ProjectR.
 */
public interface IModule {

    boolean init();

    void registerCommands();

    void stop();

}
