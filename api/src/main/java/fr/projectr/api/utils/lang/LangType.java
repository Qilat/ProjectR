package fr.projectr.api.utils.lang;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 14/08/2017.
 */
public enum LangType {
    fr_FR("fr_FR", "Français"),
    en_US("en_US", "English");

    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String fileName;

    @Setter(AccessLevel.PRIVATE)
    @Getter
    private String langName;

    LangType(String fileName, String langName) {
        this.setFileName(fileName);
        this.setLangName(langName);
    }

    /**
     * Return the LangType class thanks to fileName value
     *
     * @param testValue
     * @return LangType if found - null if not
     */
    public static LangType getByFileName(String testValue) {
        for (LangType lt : LangType.values())
            if (lt.getFileName().toUpperCase().equals(testValue.toUpperCase()))
                return lt;
        return null;
    }
}
