package fr.projectr.api.net;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class AuthResponse {
    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Type type;

    public AuthResponse(){}

    public AuthResponse(String name, Type type){
        this.name = name;
        this.type = type;
    }

    public enum Type{
        ACCEPT,
        DENY
    }
}
