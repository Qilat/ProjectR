package fr.projectr.api.net;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Qilat on 30/09/2017 for ProjectR.
 */
public class AuthRequest {
    @Setter
    @Getter
    String name;
    @Setter
    @Getter
    String password;

    public AuthRequest(){}

    public AuthRequest(String name, String password){
        this.name = name;
        this.password = password;
    }
}
