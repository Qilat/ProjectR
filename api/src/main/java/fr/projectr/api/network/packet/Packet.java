package fr.projectr.api.network.packet;

public abstract class Packet {

    public Packet() {
    }

    public abstract void read(String[] args);

    public abstract String[] write();

    public abstract void act();

}
