package fr.projectr.api.network.listener;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import fr.projectr.api.network.packet.PacketHandler;

/**
 * Created by Theophile on 17/11/2018 for ProjectR.
 */
public class ReceivedListener extends Listener {

    @Override
    public void received(Connection connection, Object object) {
        System.out.println("Packet = " + object);
        if (object instanceof String)
            PacketHandler.get().receivedPacket((String) object);
    }
}
