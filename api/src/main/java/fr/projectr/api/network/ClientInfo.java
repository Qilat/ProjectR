package fr.projectr.api.network;

import com.esotericsoftware.kryonet.Connection;
import lombok.Getter;
import lombok.Setter;

public class ClientInfo {

    @Getter
    @Setter
    int connectionID;

    @Getter
    @Setter
    Connection connection;

    public ClientInfo(int connectionID, Connection connection) {
        this.connectionID = connectionID;
        this.connection = connection;
    }
}
