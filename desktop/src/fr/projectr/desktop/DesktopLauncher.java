package fr.projectr.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import fr.projectr.core.client.ProjectRClient;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "ProjectRClient";
        config.useGL30 = false;

        //WINDOWED
        config.width = 1280;
        config.height = 720;
        //TO POSITION THE WINDOW : -1 to center
        config.x = -1;
        config.y = -1;
        config.vSyncEnabled = true;

        new LwjglApplication(new ProjectRClient(), config);
    }
}
